truncate table network.cand_hco;
truncate table network.cand_hcp;
truncate table network.cand_license;
truncate table network.cand_address;
truncate table network.cand_parenthco;


insert into network.cand_hco
select * from network.hco where candidate_record__v = 'true';

insert into network.cand_hcp
select * from network.hcp where candidate_record__v = 'true';

insert into network.cand_license
select a.* from network.license a
inner join network.cand_hcp b on a.entity_vid__v = b.vid__v;

insert into network.cand_license
select a.* from network.license a
inner join network.cand_hco b on a.entity_vid__v = b.vid__v;


insert into network.cand_address
select a.* from network.address a
inner join network.cand_hcp b on a.entity_vid__v = b.vid__v;

insert into network.cand_address
select a.* from network.address a
inner join network.cand_hco b on a.entity_vid__v = b.vid__v;

insert into network.cand_parenthco
select a.* from network.parenthco a
inner join network.cand_hcp b on a.entity_vid__v = b.vid__v;

insert into network.cand_parenthco
select a.* from network.parenthco a
inner join network.cand_hco b on a.entity_vid__v = b.vid__v;

DELETE from network.parenthco a
using network.cand_parenthco b
where a.vid__v = b.vid__v;

DELETE from network.license a
USING network.cand_license b
where a.vid__v = b.vid__v;

DELETE from network.address a
USING network.cand_address b
where a.vid__v = b.vid__v;

delete from network.hcp where candidate_record__v = 'true';

delete from network.hco where candidate_record__v = 'true';
