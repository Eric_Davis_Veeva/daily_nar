use network;
drop table if exists tmpaddr;
create table tmpaddr (vid bigint(18),entity bigint(18),ordinal int,rnk int);

alter table tmpaddr add index(entity,rnk);

insert into tmpaddr
select
vid__v as vid,entity_vid__v as entity,address_ordinal__v,CASE a.entity_vid__v
                     WHEN @id THEN @rnk := @rnk + 1
                     ELSE @rnk := 1 AND @id := a.entity_vid__v
                  END as fixrnk
from network.address a, (SELECT @rnk := 0, @id := 0) tmp
where record_state__v = 'VALID'
and address_line_1__v is not null
order by entity_vid__v,address_status__v,address_ordinal__v ASC;

drop table if exists v_best_address_tmp;
create table v_best_address_tmp (entity bigint(18), vid bigint(18));

alter table v_best_address_tmp add index(entity,vid);

drop table if exists tt;
create table tt as
select bb.entity,min(rnk) as rnk from tmpaddr bb
group by bb.entity
ORDER BY NULL;

insert into v_best_address_tmp
select a.entity, vid from tmpaddr a
inner join tt b on a.entity = b.entity and a.rnk = b.rnk;


-- alter table v_best_address add index(entity,vid);
drop table if exists v_best_address;
rename table v_best_address_tmp to v_best_address;

drop table if exists tmpaddr;
drop table if exists tt;
