USE network;
LOAD DATA local infile 'tmp/reference.csv'
INTO TABLE tmp_reference
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
lines terminated by '\n'
IGNORE 1 LINES
;
SHOW WARNINGS
;
