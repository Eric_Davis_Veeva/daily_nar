USE network;
/*drop table if exists tmp_parenthco;
create table tmp_parenthco as select * from parenthco where 1=0;
alter table tmp_parenthco add primary key(vid__v);
*/
LOAD DATA local infile 'tmp/parenthco.csv'
INTO TABLE tmp_parenthco
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
lines terminated by '\n'
IGNORE 1 LINES
(@contract_number__v,@created_date__v,@custom_keys__v,@department_class__v,@email_10__v,@email_1__v,@email_2__v,@email_3__v,@email_4__v,@email_5__v,@email_6__v,@email_7__v,@email_8__v,@email_9__v,@end_date__v,@entity_type__v,@entity_vid__v,@fax_10__v,@fax_1__v,@fax_2__v,@fax_3__v,@fax_4__v,@fax_5__v,@fax_6__v,@fax_7__v,@fax_8__v,@fax_9__v,@hierarchy_type__v,internal_dcr_anchor__c,@is_externally_mastered__v,@is_primary_relationship__v,@is_proprietary__v,is_veeva_master__v,issue_tracking_id__c,modified_date__v,@parent_hco_status__v,@parent_hco_vid__v,@parent_type__v,@phone_10__v,@phone_1__v,@phone_2__v,@phone_3__v,@phone_4__v,@phone_5__v,@phone_6__v,@phone_7__v,@phone_8__v,@phone_9__v,@record_delta_id__v,record_merged_vid__v,@record_state__v,@relationship_type__v,@start_date__v,@status_update_time__v,vid__v)
SET
contract_number__v = nullif(@contract_number__v,''),
created_date__v = nullif(@created_date__v,''),
custom_keys__v = nullif(@custom_keys__v,''),
department_class__v = nullif(@department_class__v,''),
email_10__v = nullif(@email_10__v,''),
email_1__v = nullif(@email_1__v,''),
email_2__v = nullif(@email_2__v,''),
email_3__v = nullif(@email_3__v,''),
email_4__v = nullif(@email_4__v,''),
email_5__v = nullif(@email_5__v,''),
email_6__v = nullif(@email_6__v,''),
email_7__v = nullif(@email_7__v,''),
email_8__v = nullif(@email_8__v,''),
email_9__v = nullif(@email_9__v,''),
end_date__v = nullif(@end_date__v,''),
entity_type__v = nullif(@entity_type__v,''),
entity_vid__v = nullif(@entity_vid__v,''),
fax_10__v = nullif(@fax_10__v,''),
fax_1__v = nullif(@fax_1__v,''),
fax_2__v = nullif(@fax_2__v,''),
fax_3__v = nullif(@fax_3__v,''),
fax_4__v = nullif(@fax_4__v,''),
fax_5__v = nullif(@fax_5__v,''),
fax_6__v = nullif(@fax_6__v,''),
fax_7__v = nullif(@fax_7__v,''),
fax_8__v = nullif(@fax_8__v,''),
fax_9__v = nullif(@fax_9__v,''),
hierarchy_type__v = nullif(@hierarchy_type__v,''),
is_externally_mastered__v = nullif(@is_externally_mastered__v,''),
is_primary_relationship__v = nullif(@is_primary_relationship__v,''),
is_proprietary__v = nullif(@is_proprietary__v,''),
parent_hco_status__v = nullif(@parent_hco_status__v,''),
parent_hco_vid__v = nullif(@parent_hco_vid__v,''),
parent_type__v = nullif(@parent_type__v,''),
phone_10__v = nullif(@phone_10__v,''),
phone_1__v = nullif(@phone_1__v,''),
phone_2__v = nullif(@phone_2__v,''),
phone_3__v = nullif(@phone_3__v,''),
phone_4__v = nullif(@phone_4__v,''),
phone_5__v = nullif(@phone_5__v,''),
phone_6__v = nullif(@phone_6__v,''),
phone_7__v = nullif(@phone_7__v,''),
phone_8__v = nullif(@phone_8__v,''),
phone_9__v = nullif(@phone_9__v,''),
record_delta_id__v = nullif(@record_delta_id__v,''),
record_state__v = nullif(@record_state__v,''),
relationship_type__v = nullif(@relationship_type__v,''),
start_date__v = nullif(@start_date__v,''),
status_update_time__v = nullif(@status_update_time__v,'');
SHOW WARNINGS;
