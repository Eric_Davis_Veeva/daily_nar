USE network;
DROP TABLE IF EXISTS tp_start, t1, t2, t3, t4, t5, t6, t7, v_top_parent_new;
-- get all the bottom level entities

CREATE TABLE tp_start
AS
   SELECT DISTINCT a.entity_vid__v
     FROM network.parenthco a
    WHERE     a.parent_hco_status__v = 'A'
          AND a.record_state__v = 'Valid'
          AND hierarchy_type__v = 'HCO_HCO';

CREATE INDEX x
   ON tp_start(entity_vid__v);

CREATE TABLE t1
AS
   SELECT a.entity_vid__v, b.parent_hco_vid__v AS t1_parent
     FROM tp_start a
          LEFT JOIN network.parenthco b
             ON     a.entity_vid__v = b.entity_vid__v
		AND b.record_state__v = 'Valid'
                AND b.parent_hco_status__v = 'A'
                AND b.relationship_type__v NOT IN ('340',
                                                   '7340',
                                                   '7341',
                                                   '7343',
                                                   '7344',
                                                   '7345',
                                                   '7346',
                                                   '7347',
                                                   '7349',
                                                   '7350',
                                                   '7351',
                                                   '7354',
                                                   '7356',
                                                   '7358',
                                                   '7559');
CREATE TABLE t2
AS
   SELECT a.*, b.parent_hco_vid__v AS t2_parent
     FROM t1 a
          LEFT JOIN network.parenthco b
             ON     a.t1_parent = b.entity_vid__v
	AND b.record_state__v = 'Valid'
                AND b.parent_hco_status__v = 'A'
                AND b.relationship_type__v NOT IN ('340',
                                                   '7340',
                                                   '7341',
                                                   '7343',
                                                   '7344',
                                                   '7345',
                                                   '7346',
                                                   '7347',
                                                   '7349',
                                                   '7350',
                                                   '7351',
                                                   '7354',
                                                   '7356',
                                                   '7358',
                                                   '7559')
                AND b.parent_hco_vid__v <> t1_parent;


CREATE TABLE t3
AS
   SELECT a.*, b.parent_hco_vid__v AS t3_parent
     FROM t2 a
          LEFT JOIN network.parenthco b
             ON     a.t2_parent = b.entity_vid__v
		AND b.record_state__v = 'Valid'
                AND b.parent_hco_status__v = 'A'
                AND b.relationship_type__v NOT IN ('340',
                                                   '7340',
                                                   '7341',
                                                   '7343',
                                                   '7344',
                                                   '7345',
                                                   '7346',
                                                   '7347',
                                                   '7349',
                                                   '7350',
                                                   '7351',
                                                   '7354',
                                                   '7356',
                                                   '7358',
                                                   '7559')
                AND (    b.parent_hco_vid__v <> t1_parent
                     AND b.parent_hco_vid__v <> t2_parent);

CREATE TABLE t4
AS
   SELECT a.*, b.parent_hco_vid__v AS t4_parent
     FROM t3 a
          LEFT JOIN network.parenthco b
             ON     a.t3_parent = b.entity_vid__v
	AND b.record_state__v = 'Valid'
                AND b.parent_hco_status__v = 'A'
                AND b.relationship_type__v NOT IN ('340',
                                                   '7340',
                                                   '7341',
                                                   '7343',
                                                   '7344',
                                                   '7345',
                                                   '7346',
                                                   '7347',
                                                   '7349',
                                                   '7350',
                                                   '7351',
                                                   '7354',
                                                   '7356',
                                                   '7358',
                                                   '7559')
                AND (    b.parent_hco_vid__v <> t1_parent
                     AND b.parent_hco_vid__v <> t2_parent
                     AND b.parent_hco_vid__v <> t3_parent);


CREATE TABLE t5
AS
   SELECT a.*, b.parent_hco_vid__v AS t5_parent
     FROM t4 a
          LEFT JOIN network.parenthco b
             ON     a.t4_parent = b.entity_vid__v
	AND b.record_state__v = 'Valid'
                AND b.parent_hco_status__v = 'A'
                AND b.relationship_type__v NOT IN ('340',
                                                   '7340',
                                                   '7341',
                                                   '7343',
                                                   '7344',
                                                   '7345',
                                                   '7346',
                                                   '7347',
                                                   '7349',
                                                   '7350',
                                                   '7351',
                                                   '7354',
                                                   '7356',
                                                   '7358',
                                                   '7559')
                AND (    b.parent_hco_vid__v <> t1_parent
                     AND b.parent_hco_vid__v <> t2_parent
                     AND b.parent_hco_vid__v <> t3_parent
                     AND b.parent_hco_vid__v <> t4_parent);

CREATE TABLE t6
AS
   SELECT a.*, b.parent_hco_vid__v AS t6_parent
     FROM t5 a
          LEFT JOIN network.parenthco b
             ON     a.t5_parent = b.entity_vid__v
	AND b.record_state__v = 'Valid'
                AND b.parent_hco_status__v = 'A'
                AND b.relationship_type__v NOT IN ('340',
                                                   '7340',
                                                   '7341',
                                                   '7343',
                                                   '7344',
                                                   '7345',
                                                   '7346',
                                                   '7347',
                                                   '7349',
                                                   '7350',
                                                   '7351',
                                                   '7354',
                                                   '7356',
                                                   '7358',
                                                   '7559')
                AND (    b.parent_hco_vid__v <> t1_parent
                     AND b.parent_hco_vid__v <> t2_parent
                     AND b.parent_hco_vid__v <> t3_parent
                     AND b.parent_hco_vid__v <> t4_parent
                     AND b.parent_hco_vid__v <> t5_parent);

CREATE TABLE t7
AS
   SELECT a.*, b.parent_hco_vid__v AS t7_parent
     FROM t6 a
          LEFT JOIN network.parenthco b
             ON     a.t6_parent = b.entity_vid__v
	AND b.record_state__v = 'Valid'
                AND b.parent_hco_status__v = 'A'
                AND b.relationship_type__v NOT IN ('340',
                                                   '7340',
                                                   '7341',
                                                   '7343',
                                                   '7344',
                                                   '7345',
                                                   '7346',
                                                   '7347',
                                                   '7349',
                                                   '7350',
                                                   '7351',
                                                   '7354',
                                                   '7356',
                                                   '7358',
                                                   '7559')
                AND (    b.parent_hco_vid__v <> t1_parent
                     AND b.parent_hco_vid__v <> t2_parent
                     AND b.parent_hco_vid__v <> t3_parent
                     AND b.parent_hco_vid__v <> t4_parent
                     AND b.parent_hco_vid__v <> t5_parent
                     AND b.parent_hco_vid__v <> t6_parent);

ALTER TABLE t7
   ADD COLUMN top_parent bigint(18);

UPDATE t7
   SET top_parent = entity_vid__v;

UPDATE t7
   SET top_parent = t1_parent
 WHERE t1_parent IS NOT NULL;

UPDATE t7
   SET top_parent = t2_parent
 WHERE t2_parent IS NOT NULL;

UPDATE t7
   SET top_parent = t3_parent
 WHERE t3_parent IS NOT NULL;

UPDATE t7
   SET top_parent = t4_parent
 WHERE t4_parent IS NOT NULL;

UPDATE t7
   SET top_parent = t5_parent
 WHERE t5_parent IS NOT NULL;

UPDATE t7
   SET top_parent = t6_parent
 WHERE t6_parent IS NOT NULL;

UPDATE t7
   SET top_parent = t7_parent
 WHERE t7_parent IS NOT NULL;

drop table if exists v_top_parent_all;
create table v_top_parent_all as
select distinct * from t7;

CREATE TABLE v_top_parent_new
AS
   SELECT DISTINCT entity_vid__v AS entity, top_parent
     FROM t7;

ALTER TABLE v_top_parent_new ADD INDEX(entity),
 ADD INDEX(top_parent);

ALTER TABLE v_top_parent_all ADD INDEX(entity_vid__v),
 ADD INDEX(top_parent);

DROP TABLE if exists v_top_parent;

RENAME TABLE v_top_parent_new TO v_top_parent;

-- clean up
 DROP TABLE IF EXISTS tp_start, t1, t2, t3, t4, t5, t6, t7;
