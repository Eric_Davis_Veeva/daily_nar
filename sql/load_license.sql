USE network;
/*drop table if exists tmp_license;
create table tmp_license as select * from license where 1=0;
alter table tmp_license add primary key(vid__v);*/
LOAD DATA local infile 'tmp/license.csv'
INTO TABLE tmp_license
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
lines terminated by '\n'
IGNORE 1 LINES
(@address_vid__v,@address_vid_key,@anticipated_expiry_date__v,@best_state_license__v,@body__v,@created_date__v,@custom_keys__v,@dea_business_activity_code__v,@dea_payment_indicator__v,@drug_schedule__v,@effective_date__v,@entity_type__v,entity_vid__v,@expiration_date__v,@grace_period__v,@initial_board_license_date__v,internal_dcr_anchor__c,@is_externally_mastered__v,@is_proprietary__v,is_veeva_master__v,issue_tracking_id__c,@license_degree__v,@license_eligibility__v,@license_number__v,@license_status__v,@license_status_condition__v,@license_subtype__v,@licensing_body__v,modified_date__v,@record_delta_id__v,@record_merged_vid__v,@record_state__v,@rxa_eligible__v,@sl_credentials_vod__c,@sln_template_vod__c,@status_update_time__v,@type__v,@type_value__v,vid__v)
SET
address_vid__v = nullif(@address_vid__v,''),
address_vid_key = nullif(@address_vid_key,''),
anticipated_expiry_date__v = nullif(@anticipated_expiry_date__v,''),
best_state_license__v = nullif(@best_state_license__v,''),
body__v = nullif(@body__v,''),
created_date__v = nullif(@created_date__v,''),
custom_keys__v = nullif(@custom_keys__v,''),
dea_business_activity_code__v = nullif(@dea_business_activity_code__v,''),
dea_payment_indicator__v  = nullif(@dea_payment_indicator__v,''),
drug_schedule__v = nullif(@drug_schedule__v,''),
effective_date__v = nullif(@effective_date__v,''),
entity_type__v = nullif(@entity_type__v,''),
expiration_date__v = nullif(@expiration_date__v,''),
grace_period__v = nullif(@grace_period__v,''),
initial_board_license_date__v = nullif(@initial_board_license_date__v,''),
is_externally_mastered__v = nullif(@is_externally_mastered__v,''),
is_proprietary__v = nullif(@is_proprietary__v,''),
license_degree__v = nullif(@license_degree__v,''),
license_eligibility__v = nullif(@license_eligibility__v,''),
license_number__v = nullif(@license_number__v,''),
license_status__v = nullif(@license_status__v,''),
license_status_condition__v = nullif(@license_status_condition__v,''),
license_subtype__v = nullif(@license_subtype__v,''),
licensing_body__v = nullif(@licensing_body__v,''),
record_delta_id__v = nullif(@record_delta_id__v,''),
record_merged_vid__v = if(@record_merged_vid__v,'',-1),
record_state__v = nullif(@record_state__v,''),
rxa_eligible__v = nullif(@rxa_eligible__v,''),
sl_credentials_vod__c = nullif(@sl_credentials_vod__c,''),
sln_template_vod__c = nullif(@sln_template_vod__c,''),
status_update_time__v = nullif(@status_update_time__v,''),
type__v = nullif(@type__v,''),
type_value__v = nullif(@type_value__v,'');
SHOW WARNINGS;

