USE network;
/*drop table if exists tmp_hco;
create table tmp_hco as select * from hco where 1=0;
alter table tmp_hco add primary key(vid__v);*/
LOAD DATA local infile 'tmp/hco.csv'
INTO TABLE tmp_hco
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
lines terminated by '\n'
IGNORE 1 LINES
(@340B_eligible__v,@340B_id_1__v,@340B_id_2__v,@accept_medicaid__v,@accept_medicare__v,@addresses__v,@aha_id__v,@alternate_name_1__v,@ama_do_not_contact__v,@ams_id__v,@candidate_record__v,@ccn_id__v,@cdc_flag_vod__c,@clia_effective_date__v,@clia_exception_date__v,@clia_lab_num__v,@clia_status__v,@corporate_name__v,@count_all_locn_md_do__v,@count_all_locn_medstaff__v,@count_all_locn_non_md_do__v,@count_beds__v,@count_discharged_patients__v,@count_employees__v,@count_inpatients__v,@count_licensed_asst_drs__v,@count_licensed_drs__v,@count_md_do__v,@count_medstaff__v,@count_non_md_do__v,@count_outpatients__v,@count_patients__v,@created_date__v,cri_id__v,@custom_keys__v,@cycle_id_vod__c,@department_class__v,@email_10__v,@email_1__v,@email_2__v,@email_3__v,@email_4__v,@email_5__v,@email_6__v,@email_7__v,@email_8__v,@email_9__v,@emr_installation_date__v,@emr_system_name__v,@established_date__v,@fax_10__v,@fax_1__v,@fax_2__v,@fax_3__v,@fax_4__v,@fax_5__v,@fax_6__v,@fax_7__v,@fax_8__v,@fax_9__v,@gln_id__v,@hco_status__v,@hco_tax_id__v,@hco_type__v,@hin__v,@hospital_grade__v,internal_dcr_anchor__c,@is_externally_mastered__v,@is_proprietary__v,@is_veeva_master__v,issue_tracking_id__c,@kaiser__v,@lab_services__v,@licenses__v,@major_class_of_trade__v,@master_vid__v,@modified_date__v,@national_id__v,@ncpdp_num__v,@npi_num__v,@organization_id__v,@pac_id__v,@parent_hcos__v,@percent_medicaid__v,@percent_medicare__v,@phone_10__v,@phone_1__v,@phone_2__v,@phone_3__v,@phone_4__v,@phone_5__v,@phone_6__v,@phone_7__v,@phone_8__v,@phone_9__v,@pinyin_name__v,@primary_country__v,@record_delta_id__v,@record_merged_vid__v,@record_state__v,@record_version__v,@roster_date__v,sanction_address_1__v,sanction_address_2__v,sanction_address_3__v,sanction_corporate_name_1__v,sanction_corporate_name_2__v,sanction_corporate_name_3__v,sanction_entity_type_1__v,sanction_entity_type_2__v,sanction_entity_type_3__v,sanction_exclusion_category_1__v,sanction_exclusion_category_2__v,sanction_exclusion_category_3__v,sanction_exclusion_date_1__v,sanction_exclusion_date_2__v,sanction_exclusion_date_3__v,sanction_exclusion_description_1__v,sanction_exclusion_description_2__v,sanction_exclusion_description_3__v,sanction_npi_1__v,sanction_npi_2__v,sanction_npi_3__v,sanction_reinstatement_date_1__v,sanction_reinstatement_date_2__v,sanction_reinstatement_date_3__v,sanction_specialty_1__v,sanction_specialty_2__v,sanction_specialty_3__v,sanction_upin_1__v,sanction_upin_2__v,sanction_upin_3__v,sanction_waiver_date_1__v,sanction_waiver_date_2__v,sanction_waiver_date_3__v,sanction_waiver_state_1__v,sanction_waiver_state_2__v,sanction_waiver_state_3__v,@sha_id__v,@specialty_10__v,@specialty_10_rank__v,@specialty_1__v,@specialty_1_rank__v,@specialty_2__v,@specialty_2_rank__v,@specialty_3__v,@specialty_3_rank__v,@specialty_4__v,@specialty_4_rank__v,@specialty_5__v,@specialty_5_rank__v,@specialty_6__v,@specialty_6_rank__v,@specialty_7__v,@specialty_7_rank__v,@specialty_8__v,@specialty_8_rank__v,@specialty_9__v,@specialty_9_rank__v,@status_update_time__v,@total_revenue__v,@total_revenue_currency__v,@training_facility__v,@URL_1__v,@URL_2__v,@va_dod_affiliated__v,vid__v,@xray_services__v)
SET
340B_eligible__v = nullif(@340B_eligible__v,''),
340B_id_1__v = nullif(@340B_id_1__v,''),
340B_id_2__v = nullif(@340B_id_2__v,''),
ccn_id__v = nullif(@ccn_id__v,''),
pac_id__v = nullif(@pac_id__v,''),
accept_medicaid__v = nullif(@accept_medicaid__v,''),
accept_medicare__v = nullif(@accept_medicare__v,''),
addresses__v = nullif(@addresses__v,''),
alternate_name_1__v = nullif(@alternate_name_1__v,''),
ama_do_not_contact__v = nullif(@ama_do_not_contact__v,''),
ams_id__v = nullif(@ams_id__v,''),
candidate_record__v = if(@candidate_record__v = '','FALSE',@candidate_record__v),
cdc_flag_vod__c = nullif(@cdc_flag_vod__c,''),
clia_effective_date__v = nullif(@clia_effective_date__v,''),
clia_exception_date__v = nullif(@clia_exception_date__v,''),
clia_lab_num__v = nullif(@clia_lab_num__v,''),
clia_status__v = nullif(@clia_status__v,''),
corporate_name__v = nullif(@corporate_name__v,''),
count_all_locn_md_do__v = nullif(@count_all_locn_md_do__v,''),
count_all_locn_medstaff__v = nullif(@count_all_locn_medstaff__v,''),
count_all_locn_non_md_do__v = nullif(@count_all_locn_non_md_do__v,''),
count_beds__v = nullif(@count_beds__v,''),
count_discharged_patients__v = nullif(@count_discharged_patients__v,''),
count_employees__v = nullif(@count_employees__v,''),
count_inpatients__v = nullif(@count_inpatients__v,''),
count_licensed_asst_drs__v = nullif(@count_licensed_asst_drs__v,''),
count_licensed_drs__v = nullif(@count_licensed_drs__v,''),
count_md_do__v = nullif(@count_md_do__v,''),
count_medstaff__v = nullif(@count_medstaff__v,''),
count_non_md_do__v = nullif(@count_non_md_do__v,''),
count_outpatients__v = nullif(@count_outpatients__v,''),
count_patients__v = nullif(@count_patients__v,''),
created_date__v = nullif(@created_date__v,''),
custom_keys__v = nullif(@custom_keys__v,''),
cycle_id_vod__c = nullif(@cycle_id_vod__c,''),
department_class__v = nullif(@department_class__v,''),
email_10__v = nullif(@email_10__v,''),
email_1__v = nullif(@email_1__v,''),
email_2__v = nullif(@email_2__v,''),
email_3__v = nullif(@email_3__v,''),
email_4__v = nullif(@email_4__v,''),
email_5__v = nullif(@email_5__v,''),
email_6__v = nullif(@email_6__v,''),
email_7__v = nullif(@email_7__v,''),
email_8__v = nullif(@email_8__v,''),
email_9__v = nullif(@email_9__v,''),
emr_installation_date__v = nullif(@emr_installation_date__v,''),
emr_system_name__v = nullif(@emr_system_name__v,''),
established_date__v = nullif(@established_date__v,''),
fax_10__v = nullif(@fax_10__v,''),
fax_1__v = nullif(@fax_1__v,''),
fax_2__v = nullif(@fax_2__v,''),
fax_3__v = nullif(@fax_3__v,''),
fax_4__v = nullif(@fax_4__v,''),
fax_5__v = nullif(@fax_5__v,''),
fax_6__v = nullif(@fax_6__v,''),
fax_7__v = nullif(@fax_7__v,''),
fax_8__v = nullif(@fax_8__v,''),
fax_9__v = nullif(@fax_9__v,''),
gln_id__v = nullif(@gln_id__v,''),
hco_status__v = nullif(@hco_status__v,''),
hco_tax_id__v = nullif(@hco_tax_id__v,''),
hco_type__v = nullif(@hco_type__v,''),
hin__v = nullif(@hin__v,''),
hospital_grade__v = nullif(@hospital_grade__v,''),
is_externally_mastered__v = nullif(@is_externally_mastered__v,''),
is_proprietary__v = nullif(@is_proprietary__v,''),
is_veeva_master__v = nullif(@is_veeva_master__v,''),
kaiser__v = nullif(@kaiser__v,''),
lab_services__v = nullif(@lab_services__v,''),
licenses__v = nullif(@licenses__v,''),
major_class_of_trade__v = nullif(@major_class_of_trade__v,''),
master_vid__v = nullif(@master_vid__v,''),
modified_date__v = nullif(@modified_date__v,''),
national_id__v = nullif(@national_id__v,''),
ncpdp_num__v = nullif(@ncpdp_num__v,''),
npi_num__v = nullif(@npi_num__v,''),
organization_id__v = nullif(@organization_id__v,''),
parent_hcos__v = nullif(@parent_hcos__v,''),
percent_medicaid__v = nullif(@percent_medicaid__v,''),
percent_medicare__v = nullif(@percent_medicare__v,''),
phone_10__v = nullif(@phone_10__v,''),
phone_1__v = nullif(@phone_1__v,''),
phone_2__v = nullif(@phone_2__v,''),
phone_3__v = nullif(@phone_3__v,''),
phone_4__v = nullif(@phone_4__v,''),
phone_5__v = nullif(@phone_5__v,''),
phone_6__v = nullif(@phone_6__v,''),
phone_7__v = nullif(@phone_7__v,''),
phone_8__v = nullif(@phone_8__v,''),
phone_9__v = nullif(@phone_9__v,''),
pinyin_name__v = nullif(@pinyin_name__v,''),
primary_country__v = nullif(@primary_country__v,''),
record_delta_id__v = nullif(@record_delta_id__v,''),
record_merged_vid__v = if(@record_merged_vid__v='',-1,@record_merged_vid__v),
record_state__v = nullif(@record_state__v,''),
record_version__v = nullif(@record_version__v,''),
roster_date__v = nullif(@roster_date__v,''),
sha_id__v = nullif(@sha_id__v,''),
specialty_10__v = nullif(@specialty_10__v,''),
specialty_10_rank__v = nullif(@specialty_10_rank__v,''),
specialty_1__v = nullif(@specialty_1__v,''),
specialty_1_rank__v = nullif(@specialty_1_rank__v,''),
specialty_2__v = nullif(@specialty_2__v,''),
specialty_2_rank__v = nullif(@specialty_2_rank__v,''),
specialty_3__v = nullif(@specialty_3__v,''),
specialty_3_rank__v = nullif(@specialty_3_rank__v,''),
specialty_4__v = nullif(@specialty_4__v,''),
specialty_4_rank__v = nullif(@specialty_4_rank__v,''),
specialty_5__v = nullif(@specialty_5__v,''),
specialty_5_rank__v = nullif(@specialty_5_rank__v,''),
specialty_6__v = nullif(@specialty_6__v,''),
specialty_6_rank__v = nullif(@specialty_6_rank__v,''),
specialty_7__v = nullif(@specialty_7__v,''),
specialty_7_rank__v = nullif(@specialty_7_rank__v,''),
specialty_8__v = nullif(@specialty_8__v,''),
specialty_8_rank__v = nullif(@specialty_8_rank__v,''),
specialty_9__v = nullif(@specialty_9__v,''),
specialty_9_rank__v = nullif(@specialty_9_rank__v,''),
status_update_time__v = nullif(@status_update_time__v,''),
total_revenue__v = nullif(@total_revenue__v,''),
total_revenue_currency__v = nullif(@total_revenue_currency__v,''),
training_facility__v = nullif(@training_facility__v,''),
URL_1__v = nullif(@URL_1__v,''),
URL_2__v = nullif(@URL_2__v,''),
va_dod_affiliated__v = nullif(@va_dod_affiliated__v,''),
xray_services__v = nullif(@xray_services__v,'');
SHOW WARNINGS;
