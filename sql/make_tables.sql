use network;
drop table if exists tmp_address;
drop table if exists tmp_license;
drop table if exists tmp_parenthco;
drop table if exists tmp_hcp;
drop table if exists tmp_hco;
drop table if exists tmp_reference;
create table tmp_address like network.address;
create table tmp_hcp like network.hcp;
create table tmp_hco like network.hco;
create table tmp_parenthco like network.parenthco;
create table tmp_license like network.license;
create table tmp_reference like network.reference;
