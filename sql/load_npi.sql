USE raw_data;
/*drop table if exists tmp_address;
create table tmp_address as select * from address where 1=0;
alter table tmp_address add primary key(vid__v);
*/
LOAD DATA local infile '/tmp/npi.csv'
INTO TABLE npi
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
lines terminated by '\n'
  IGNORE 1 LINES
