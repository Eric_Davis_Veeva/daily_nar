create table network.backup_20170206
select 
a.*,specialty_1__v,specialty_2__v,address_line_1__v, address_line_2__v, administrative_area__v,cc.phone_1__v, postal_code__v, locality__v, sub_building__v, sub_building_name__v, sub_building_number__v, premise__v, thoroughfare__v
from vidtracker.hcp a
inner join network.hcp b on a.vid__v = b.vid__v
inner join network.v_best_address c on b.vid__v = c.entity
inner join network.address cc on c.vid = cc.vid__v;
