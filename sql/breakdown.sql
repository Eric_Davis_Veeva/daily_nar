-- delete from vidtracker.dcr where owner = 'vinodhkumar.ramachandran@usmaster.veevanetwork.com';

TRUNCATE TABLE vidtracker.breakdown;
/*
NPI
*/

-- Has NPI

INSERT INTO vidtracker.breakdown
  SELECT DISTINCT vid__v, 'NPI' AS type, "Has" AS status
  FROM   vidtracker.vid_tracker
  WHERE  has_npi = 1;
-- Confirmed has no NPI

INSERT INTO vidtracker.breakdown
  SELECT DISTINCT vid__v, 'NPI' AS type, "Confirmed Has None" AS status
  FROM   vidtracker.vid_tracker
  WHERE  has_npi = 0 AND no_npi = 1;

-- Pending in PDR

INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'NPI' AS type, 'Pending PDR' AS status
  FROM   vidtracker.vid_tracker  a
         INNER JOIN vidtracker.dcr b ON a.vid__v = b.vid__v AND b.resolution = "CHANGE_PENDING" AND b.PDRTYPE = 'PDR0003';

-- Waiting on a NAR refresh, and not in any other status

INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'NPI' AS type, "Waiting on NAR"
  FROM   vidtracker.vid_tracker  a
         INNER JOIN vidtracker.dcr d
           ON a.vid__v = d.vid__v AND d.PDRTYPE = 'PDR0003' AND d.resolution IN ("CHANGE_ACCEPTED", "CHANGE_PARTIAL") AND left(d.resolution_notes, 9)
              = '[A-10007]'
         LEFT JOIN vidtracker.breakdown b ON a.vid__v = b.vid__v AND type = 'NPI'
  WHERE  b.vid__v IS NULL;

-- Needs PDR

INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'NPI' AS type, "Needs PDR"
  FROM   vidtracker.vid_tracker a LEFT JOIN vidtracker.breakdown b ON a.vid__v = b.vid__v AND type = 'NPI'
  WHERE  b.vid__v IS NULL AND has_npi = 0;

/*
STLIC
*/
-- Has 
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT vid__v, 'STLIC' AS type, "Has" AS status
  FROM   vidtracker.vid_tracker
  WHERE  has_state = 1;

-- Confirmed has no stlic
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT vid__v, 'STLIC' AS type, "Confirmed Has None" AS status
  FROM   vidtracker.vid_tracker
  WHERE  has_state = 0 AND no_stlic = 1;

-- Pending in PDR

INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'STLIC' AS type, 'Pending PDR' AS status
  FROM   vidtracker.vid_tracker  a
         INNER JOIN vidtracker.dcr b ON a.vid__v = b.vid__v AND b.resolution = "CHANGE_PENDING" AND b.PDRTYPE = 'PDR0002';

-- Waiting on a NAR refresh, and not in any other status

INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'STLIC' AS type, "Waiting on NAR"
  FROM   vidtracker.vid_tracker  a
         INNER JOIN vidtracker.dcr d
           ON a.vid__v = d.vid__v AND d.PDRTYPE = 'PDR0002' AND d.resolution IN ("CHANGE_ACCEPTED", "CHANGE_PARTIAL") AND left(d.resolution_notes, 9)
              = '[A-10007]'
         LEFT JOIN vidtracker.breakdown b ON a.vid__v = b.vid__v AND type = 'STLIC'
  WHERE  b.vid__v IS NULL;

-- Needs PDR
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'STLIC' AS type, "Needs PDR"
  FROM   vidtracker.vid_tracker a LEFT JOIN vidtracker.breakdown b ON a.vid__v = b.vid__v AND type = 'STLIC'
  WHERE  b.vid__v IS NULL AND has_state = 0;

/* Specialty */

-- has
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'SPECIALTY' AS type, "Has"
  FROM   vidtracker.vid_tracker a 
  where specialty_1__v is not null and specialty_1__v not in(  'Unspecified specialty','Unspecified','Other','Other Specialty');

-- Pending PDR
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'SPECIALTY' AS type, 'Pending PDR' AS status
  FROM   vidtracker.vid_tracker  a
         INNER JOIN vidtracker.dcr b ON a.vid__v = b.vid__v AND b.resolution = "CHANGE_PENDING" AND b.PDRTYPE = 'PDR0004';

-- Waiting on a NAR refresh, and not in any other status
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'SPECIALTY' AS type, "Waiting on NAR"
  FROM   vidtracker.vid_tracker  a
         INNER JOIN vidtracker.dcr d
           ON a.vid__v = d.vid__v AND d.PDRTYPE = 'PDR0004' AND d.resolution IN ("CHANGE_ACCEPTED", "CHANGE_PARTIAL") AND left(d.resolution_notes, 9)
              = '[A-10008]'
         LEFT JOIN vidtracker.breakdown b ON a.vid__v = b.vid__v AND type = 'SPECIALTY'
  WHERE  b.vid__v IS NULL;

-- Needs PDR
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'SPECIALTY' AS type, "Needs PDR"
  FROM   vidtracker.vid_tracker a LEFT JOIN vidtracker.breakdown b ON a.vid__v = b.vid__v AND type = 'SPECIALTY'
  WHERE  b.vid__v IS NULL;

/* Affiliation */

-- Has 
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT vid__v, 'AFFILIATION' AS type, "Has" AS status
  FROM   vidtracker.vid_tracker
  WHERE  has_affil = 1;

-- Pending in PDR
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'AFFILIATION' AS type, 'Pending PDR' AS status
  FROM   vidtracker.vid_tracker  a
         INNER JOIN vidtracker.dcr b ON a.vid__v = b.vid__v AND b.resolution = "CHANGE_PENDING" AND b.PDRTYPE = 'PDR0001';

-- Waiting on a NAR refresh, and not in any other status
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'AFFILIATION' AS type, "Waiting on NAR"
  FROM   vidtracker.vid_tracker  a
         INNER JOIN vidtracker.dcr d
           ON a.vid__v = d.vid__v AND d.PDRTYPE = 'PDR0001' AND d.resolution IN ("CHANGE_ACCEPTED", "CHANGE_PARTIAL") AND left(d.resolution_notes, 9)
              = '[A-10017]'
         LEFT JOIN vidtracker.breakdown b ON a.vid__v = b.vid__v AND type = 'AFFILIATION'
  WHERE  b.vid__v IS NULL;

-- Needs PDR
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'AFFILIATION' AS type, "Needs PDR"
  FROM   vidtracker.vid_tracker a LEFT JOIN vidtracker.breakdown b ON a.vid__v = b.vid__v AND type = 'AFFILIATION'
  WHERE  b.vid__v IS NULL AND has_affil = 0;

/* DEA */

-- Has 
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT vid__v, 'DEA' AS type, "Has" AS status
  FROM   vidtracker.vid_tracker
  WHERE  has_dea = 1;
  
  -- Confirmed has no dea
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT vid__v, 'DEA' AS type, "Confirmed Has None" AS status
  FROM   vidtracker.vid_tracker
  WHERE  has_dea = 0 AND no_dea = 1;

-- Pending in PDR
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'DEA' AS type, 'Pending PDR' AS status
  FROM   vidtracker.vid_tracker  a
         INNER JOIN vidtracker.dcr b ON a.vid__v = b.vid__v AND b.resolution = "CHANGE_PENDING" AND b.PDRTYPE = 'PDR0008';

-- Waiting on a NAR refresh, and not in any other status
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'DEA' AS type, "Waiting on NAR"
  FROM   vidtracker.vid_tracker  a
         INNER JOIN vidtracker.dcr d
           ON a.vid__v = d.vid__v AND d.PDRTYPE = 'PDR0008' AND d.resolution IN ("CHANGE_ACCEPTED", "CHANGE_PARTIAL") AND left(d.resolution_notes, 9)
              = '[A-10007]'
         LEFT JOIN vidtracker.breakdown b ON a.vid__v = b.vid__v AND type = 'DEA'
  WHERE  b.vid__v IS NULL;

-- Needs PDR
INSERT INTO vidtracker.breakdown
  SELECT DISTINCT a.vid__v, 'DEA' AS type, "Needs PDR"
  FROM   vidtracker.vid_tracker a LEFT JOIN vidtracker.breakdown b ON a.vid__v = b.vid__v AND type = 'DEA'
  WHERE  b.vid__v IS NULL AND has_dea = 0;



