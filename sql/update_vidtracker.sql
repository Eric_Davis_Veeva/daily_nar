
delete from DCR.All_DCR where owner = 'vinodhkumar.ramachandran@usmaster.veevanetwork.com';

-- Update 'NO' tables to use the winner vids always 

-- DEA
update  vidtracker.no_dea a
inner join network.hcp b on a.vid__v = b.vid__v
set a.vid__v = b.record_merged_vid__v
where b.record_state__v <> 'VALID';

-- NPI
update  vidtracker.no_npi a
inner join network.hcp b on a.vid__v = b.vid__v
set a.vid__v = b.record_merged_vid__v
where b.record_state__v <> 'VALID';

-- STLIC
update  vidtracker.no_stlic a
inner join network.hcp b on a.vid__v = b.vid__v
set a.vid__v = b.record_merged_vid__v
where b.record_state__v <> 'VALID';

-- Get merged records for the main table
drop table if exists vidtracker.tmp1;
create table vidtracker.tmp1 as
select distinct a.*,b.record_state__v,b.record_merged_vid__v from vidtracker.hcp a
inner join network.hcp b on a.vid__v = b.vid__v
where b.record_state__v = 'MERGED_INTO';

-- delete the loser records from the table
delete a.* from vidtracker.hcp a
inner join vidtracker.tmp1 b on a.vid__v = b.vid__v;

-- delete any already winner vids from the new table already in the vidtracker table & client_id
delete a from vidtracker.tmp1 a
inner join(
select vid__v,client_id from vidtracker.hcp
) b on  a.record_merged_vid__v = b.vid__v and a.client_id = b.client_id;

-- insert the winner records
insert into vidtracker.hcp
select distinct record_merged_vid__v,client_id from vidtracker.tmp1;

-- Generate a temp table of per vid client counts for use
-- Dropped the concept of batch number
drop table if exists vidtracker.temp1;

create table vidtracker.temp1 as
-- explain extended
select vid__v,count(client_id) as cnt
from vidtracker.hcp a
group by vid__v;

alter table vidtracker.temp1 add index(vid__v,cnt);
-- cleanup
drop table if exists vidtracker.tmp1;

drop table if exists vidtracker.vid_tracker;

create table vidtracker.vid_tracker
(
 vid__v bigint(18) NOT NULL PRIMARY KEY ,
cnt  int NOT NULL,
specialty_1__v varchar(50),
medical_degree_1__v varchar(10),
veevastate varchar(2),
hcp_status__v varchar(1),
no_npi int NOT NULL default 0,
no_dea int NOT NULL default 0,
no_stlic int NOT NULL default 0,
has_npi int NOT NULL default 0,
has_me int NOT NULL default 0,
has_dea int NOT NULL default 0,
has_state int NOT NULL default 0,
has_affil int NOT NULL default 0
);

insert into vidtracker.vid_tracker
-- explain extended
select distinct 
a.vid__v,
a.cnt,
e.`network name`,
b.medical_degree_1__v,
right(ff.administrative_area__v,2),
b.hcp_status__v,
0,0,0
,case when npi_num__v IS NULL then 0 else 1 END as has_npi
,case when me_id__v IS NULL then 0 else 1 END as has_me
,case when c.vid__v IS NULL then 0 else 1 END as has_dea
,case when d.vid__v IS NULL then 0 else 1 END as has_state
,0
from vidtracker.temp1 a
inner join network.hcp b on a.vid__v = b.vid__v
left join network.license c on b.vid__v = c.entity_vid__v and c.record_state__v = 'VALID' and c.type__v = 'ADDRESS'
left join network.license d on b.vid__v = d.entity_vid__v and d.record_state__v = 'VALID' and d.type__v = 'STATE'
left join network.reference e on b.specialty_1__v = e.`Network Code` and e.`Reference Type` = 'Specialty' and e.`Language Code` ='en'
inner join network.v_best_address f on a.vid__v = f.entity
inner join network.address ff on f.vid = ff.vid__v;


-- Remove records n ot used in vidtracker
delete from vidtracker.vid_tracker where medical_degree_1__v not in ('MD','DO','NP','PA','DPM','OD');
delete from vidtracker.vid_tracker where medical_degree_1__v is null;
delete from vidtracker.vid_tracker where hcp_status__v <> 'A';
delete from vidtracker.vid_tracker where veevastate in ('PR');

-- O = HBP   R = Resident
delete a.* from vidtracker.vid_tracker a
inner join network.hcp b on a.vid__v = b.vid__v
where b.hcp_type__v in('R','O');

-- Temp resident shuffle
update vidtracker.vid_tracker a
inner join network.hcp b on a.vid__v = b.vid__v
set NO_STLIC = 1
where has_state = 0
and hcp_type__v in ('R');

-- no stlic
update vidtracker.vid_tracker a
inner join vidtracker.no_stlic b on a.vid__v = b.vid__v
set no_stlic = 1 ;

-- no npi
update vidtracker.vid_tracker a
inner join vidtracker.no_npi b on a.vid__v = b.vid__v
set no_npi = 1 ;

-- no dea
update vidtracker.vid_tracker a
inner join vidtracker.no_dea b on a.vid__v = b.vid__v
set no_dea = 1 ;

-- Affill check

update vidtracker.vid_tracker a
inner join network.parenthco b on a.vid__v = b.entity_vid__v
  and b.record_state__v = 'VALID'
  and b.parent_hco_status__v = 'A'
inner join network.hco c on b.parent_hco_vid__v = c.vid__v
  and c.record_state__v = 'VALID'
  and c.hco_status__v = 'A'
set has_affil = 1 ;


