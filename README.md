# README #

# Overview #

These scripts load the Postgress database daily from a downloaded target scubscription from NAR

# Process #

* Pull target subscription compared to the last run
* Unzip files
* Clean files
* Ttruncate and load main tables
* Truncate and load candidate tables
* Remove candidated from main tables

# To Do #

* Add more robust error handling
* Stagger checks if target files not available
* Add email notifications
* convert to python ?
* Auto vacuum / analyze ?
* Query optimization ?
