#!/bin/bash
BASE="/data/opendata"
SCRIPT="${BASE}/usr/daily_nar"
WORK="${BASE}/work/"
LOG="${BASE}/logs/daily_nar.log"
SQL="${SCRIPT}/sql"
PASS=$(cat ${BASE}/env/env_psql | grep "network_user|"|sed -n -e 's/network_user|//p')
TMP="${BASE}/tmp"
export PGPASSWORD="${PASS}"
FILE="full_export_[0-9T]+\.zip"
REMOTE="/outbound/master_data_dea/full_export/"
USER=$(cat ${BASE}/env/env_conn | grep "usm_network|"|sed -n -e 's/usm_network|//p')
HOST=$(cat ${BASE}/env/env_conn | grep "usm_network|"|sed -n -e 's/.*@//p'|sed -n -e 's/,.*//p')

dirfile="${TMP}/dirfile"
email="nicholas.gemo@veeva.com,shae.williams@veeva.com,suzanne.pepito@veeva.com,bob.tierney@veeva.com,eric.davis@veeva.com,jennifer.barclay@veeva.com,gene.padilla@veeva.com,zak.rudzitskiy@veeva.com,colleen.thornton@veeva.com,paul.pearson@veeva.com,zhanna.shatson@veeva.com"

echo "DIR FILE :$dirfile"

# Pull directory listing from the FTP
lftp -e "set ssl:verify-certificate false; set ftp:ssl-force true; set ftp:ssl-protect-data true; cd ${REMOTE} ; ls; bye;" -u ${USER} ${HOST} > ${dirfile}

# get latest file
LATEST="$(tail -n 1 ${dirfile} | egrep -o "${FILE}")"

echo "Looking for $FILE"
echo "Latest refresh is $LATEST"

if [[ $LATEST == '' ]]
        then
                echo 'Problems getting latest file'
#               mail -s "*ERROR* Daily Load File" -r "Veeva ODS <do_not_reply@veeva.com>"  eric.davis@veeva.com <<< "Refresh Not loaded. Problem getting LATEST file. Last file was $LATEST"

                exit
fi


# compare this to the last downloaded file
if [[ ! -f $BASE"lastfile" ]]
        then
                echo "last File missing, touch another."
                touch "${SCRIPT}/lastfile"
                #chmod 664 $BASE"lastfile"
fi

LAST="$(cat "${SCRIPT}/lastfile" | egrep -o "$FILE")"

echo "Last downloaded file is ${LAST}"

if [[ $LAST == $LATEST ]]
        then
                echo 'No new file detected.  Exiting.'
                #mail -s "*ERROR* Daily Load File" -r "Veeva ODS <do_not_reply@veeva.com>"  eric.davis@veeva.com <<< "Refresh Not loaded.  Latest file was already downloaded. Last file was $LAST and i see $LATEST"
                exit
        else
                echo "File is different."
                echo $LATEST > "${SCRIPT}/lastfile"
fi
# Download the file

DLPATH=${WORK}${LATEST}

echo 'Begin Download'
echo "Will download to $DLPATH"

cd ${WORK}
lftp -e "set ssl:verify-certificate false; set ftp:ssl-force true; set ftp:ssl-protect-data true;cd $REMOTE ; get $LATEST; bye;" -u $USER $HOST

cd ${SCRIPT}
if [[ ! -f $DLPATH ]]
        then
                echo "No file was downloaded.   Abort"
#                mail -s "*ERROR* Daily Load File" -r "Veeva ODS <do_not_reply@veeva.com>"  eric.davis@veeva.com <<< 'Refresh Not loaded.  A new file was not downloaded.'
                exit
else
        echo 'Done Download'
fi

echo "Unzipping $LATEST file ..."
# chmod 664 $DLPATH
unzip -o $DLPATH -x manifest customkey.csv -d ${TMP}

time sed -i 's/\x00//g' "${TMP}/hco.csv"
time sed -i 's/\x00//g' "${TMP}/reference.csv"
time sed -i 's/\x00//g' "${TMP}/hcp.csv"
time sed -i 's/\x00//g' "${TMP}/parenthco.csv"
time sed -i 's/\x00//g' "${TMP}/address.csv"
time sed -i 's/\x00//g' "${TMP}/license.csv"

echo 'Truncate table'

PS="$(time psql -U network_user -d ODS < ${SQL}/pg_trunc_tables.sql)"
echo ${PS}

echo 'HCO'
PS="$(time psql -U network_user -d ODS -c '\copy network.hco from '${TMP}/hco.csv' with CSV HEADER')"
echo ${PS}

echo 'reference'
PS="$(time psql -U network_user -d ODS -c '\copy network.reference from '${TMP}/reference.csv' with CSV HEADER')"
echo ${PS}

echo 'HCP'
PS="$(time psql -U network_user -d ODS -c '\copy network.hcp from '${TMP}/hcp.csv' with CSV HEADER')"
echo ${PS}

echo 'ParentHCO'
PS="$(time psql -U network_user -d ODS -c '\copy network.parenthco from '${TMP}/parenthco.csv' with CSV HEADER')"
echo ${PS}

echo 'Address'
PS="$(time psql -U network_user -d ODS -c '\copy network.address from '${TMP}/address.csv' with CSV HEADER')"
echo ${PS}

echo 'License'
PS="$(time psql -U network_user -d ODS -c '\copy network.license from '${TMP}/license.csv' with CSV HEADER')"
echo ${PS}

echo 'Candidate table'

PS="$(time psql -U network_user -d ODS < ${SQL}/pg_cand_tables.sql)"
echo ${PS}

PS="$(time psql -U network_user -d ODS -c 'REFRESH MATERIALIZED VIEW vidtracker.vidtrackeri')"
echo ${PS}


# Clean up
rm "${TMP}/address.csv"
rm "${TMP}/parenthco.csv"
rm "${TMP}/reference.csv"
rm "${TMP}/hcp.csv"
rm "${TMP}/hco.csv"
rm "${TMP}/license.csv"

rm "${DLPATH}"

mailx -s "[PostgreSQL] PROD Refresh Complete" -S smtp=smtp://smtp.mailgun.org:587 -S smtp-auth=login -S smtp-auth-user=opendataprod@veeva.com -S smtp-auth-password=YllPD3mrhyw0UVhB2pmsUmXSOiZCS4wU -S from="ODS_PROD <admin@veevaopendata.com>" $email  <<< "PostgreSQL network tables on ODS_PROD are loaded an ready for use."

